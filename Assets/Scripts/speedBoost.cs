using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class speedBoost : MonoBehaviour
{
    public Text speedboostText;
    public int speedboostTime = 10;

    private GameObject player;
    private bool boostOver;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            StartCoroutine(SBTime());

            transform.Translate(0, -50, 0);
        }
    }

    IEnumerator SBTime()
    {
        while (speedboostTime >= 0)
        {
            player = GameObject.Find("Player");

            PlayerMov playerScript = (PlayerMov)player.GetComponent(typeof(PlayerMov));

            playerScript.speed = 10;

            speedboostText.text = "Speed boost!: " + speedboostTime;

            Debug.Log(speedboostTime);

            yield return new WaitForSeconds(1.0f);
            speedboostTime--;

            if (speedboostTime <= 0)
            {
                Debug.Log("Speed boost over.");
                speedboostText.text = "";

                playerScript.speed = 5;

            }

        }
    }
}
