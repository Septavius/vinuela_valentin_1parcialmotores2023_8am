using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovGrua : MonoBehaviour
{
    private int speed = 2;
    bool reverse = false;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        GameObject buttonGO = GameObject.Find("Button1");
        Lever buttonGOScript = (Lever)buttonGO.GetComponent(typeof(Lever)); //Busco el boton y accedo a su monobehaviour.

        bool liftStatus = buttonGOScript.liftActivate; //Accedo a la funcion para prender la grua

        if (liftStatus == true)
        {
            if (transform.position.x >= 11)
            {
                reverse = true;
            }

            if (transform.position.x <= -11)
            {
                reverse = false;
            }

            if (reverse == true)
            {
                liftGo();
            }

            else
            {
                liftBack();
            }
        }

    }
    void liftGo()
    {
        transform.position += Vector3.left * speed * Time.deltaTime;
    }

    void liftBack()
    {
        transform.position += Vector3.right * speed * Time.deltaTime;
    }    
}
