using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerMov : MonoBehaviour
{

    public Transform cam;
    public Rigidbody rb;
    public LayerMask Ground;
    private CapsuleCollider col;
    private Vector3 spawncoords;
    private Vector3 checkpointcoords;
    private Vector3 checkpoint2coords;

    private GameObject Door;
    private GameObject Gate;

    public int speed = 5;
    private int JumpForce = 6;
    float TurnSmoothTime = 0.1f;
    float VelTurnSmooth;
    private int Jumps = 1;

    public bool IM1_On = false;
    public bool IM2_On = false;
    private float InteractRange = 4.0f;

    public Text HPUI;
    public int HP = 100;

    private GameObject gameM;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        col = GetComponent<CapsuleCollider>();
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        spawncoords = transform.position;
        checkpointcoords = Vector3.zero;
        checkpoint2coords = Vector3.zero;

        setHP();

    }

    // Update is called once per frame
    void Update()
    {
        float movV = Input.GetAxis("Vertical");
        float movH = Input.GetAxis("Horizontal");

        Vector3 movi = new Vector3(movH, 0.0f, movV).normalized;


        if (movi.magnitude >= 0.1f)
        { //Si el jugador se mueve

            float DirectionTan = Mathf.Atan2(movi.x, movi.z) * Mathf.Rad2Deg + cam.eulerAngles.y; //Calculo la direcci�n del punto vectorial al cual el jugador se est� moviendo.
                                                                                                  //El �ngulo y de la camara se usa para el seguimiento del giro.

            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, DirectionTan, ref VelTurnSmooth, TurnSmoothTime); // Suavizado del giro del personaje. Se env�a como parametro
            // El �ngulo actual (EJE Y), el �ngulo objetivo (la direcci�n vectorial), una variable que guarde la velocidad de giro y el tiempo de giro.

            transform.rotation = Quaternion.Euler(0f, angle, 0f); //Aplico la direcci�n del vector sobre el eje Y del personaje.


            Vector3 dirMov = Quaternion.Euler(0f, DirectionTan, 0f) * Vector3.forward; // Movimiento en direcci�n a la c�mara (ROTACI�N EN DIR VECTORIAL Y MOV USANDO MAGNITUD EN Z)

            transform.position += dirMov * Time.deltaTime * speed;

        }

        Salto();

        Interact();

        if (Input.GetKeyDown("r"))
        {
            //transform.position = spawncoords;


            //GameObject buttonGO = GameObject.Find("Button1");

            //Lever buttonGOScript = (Lever)buttonGO.GetComponent(typeof(Lever));

            //buttonGOScript.liftActivate = false;

            //Door = GameObject.Find("Door");

            //Door.SetActive(true);

            //Gate = GameObject.Find("Gate");

            //Gate.SetActive(true);

            SceneManager.LoadScene("SampleScene");
        }

        if ((transform.position.y < -10) && IM1_On == false)
        {
            Debug.Log("Reset");
            transform.position = spawncoords;
        }

        else if ((transform.position.y < -10) && (IM1_On == true) && (IM2_On == false))
        {
            Debug.Log("Reset");
            transform.position = checkpointcoords;
        }

        else if ((transform.position.y < -10) && (IM1_On == false) && (IM2_On == true))
        {
            Debug.Log("Reset");
            transform.position = checkpoint2coords;
        }
    }
    void setHP()
    {
        HPUI.text = "HP: " + HP.ToString();

        if ((HP <= 0) && IM1_On == false)
        {
            Debug.Log("Reset");
            SceneManager.LoadScene("SampleScene");
        }
        if ((HP <= 0) && IM1_On == true)
        {
            Debug.Log("Reset");
            transform.position = spawncoords;
            HP = 100;
            setHP();
        }

    }
    private bool Grounded()
    {
        return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x, col.bounds.min.y, col.bounds.center.z), col.radius, Ground); //Check estar en piso
    }

    void Salto()
    {

        if (Input.GetKeyDown("space") && Jumps > 0)
        {
            rb.AddForce(Vector3.up * JumpForce, ForceMode.Impulse);
            Jumps--;

        }

        if (Grounded())
        {
            Jumps = 1; //Cuando vuelvo a tocar el piso, recargo los saltos.
        }

    }

    void Interact()
    {
        
        if (Input.GetKeyDown("e"))
        {
            Debug.Log("Se presiona e");

            RaycastHit hit;

            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, InteractRange))
            {
                Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow); //Test
            }

            if ((Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, InteractRange) == true) && hit.collider.name == "Button1")
            {
                Debug.Log("Toqu� bot�n ascensor");

                GameObject buttonGO = GameObject.Find(hit.transform.name);

                Lever buttonGOScript = (Lever)buttonGO.GetComponent(typeof(Lever));
                
                if (buttonGOScript != null)
                {
                    buttonGOScript.liftOn();
                }

            }
            else if ((Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, InteractRange) == true) && hit.collider.name == "Button2")
            {
                Debug.Log("Toqu� bot�n puerta");

                GameObject buttonGO = GameObject.Find(hit.transform.name);

                DoorControl buttonGOScript = (DoorControl)buttonGO.GetComponent(typeof(DoorControl));

                if (buttonGOScript != null)
                {
                    buttonGOScript.doorOpen();
                }

            }
            else if ((Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, InteractRange) == true) && hit.collider.name == "Button3")
            {
                Debug.Log("Toqu� bot�n puerta principal");

                GameObject buttonGO = GameObject.Find(hit.transform.name);

                maingateControl buttonGOScript = (maingateControl)buttonGO.GetComponent(typeof(maingateControl));

                if (buttonGOScript != null)
                {
                    buttonGOScript.MGOpen();
                }

            }
            else if ((Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, InteractRange) == true) && hit.collider.name == "Button4")
            {
                Debug.Log("Toqu� bot�n VM");

                GameObject buttonGO = GameObject.Find(hit.transform.name);

                vmControl buttonGOScript = (vmControl)buttonGO.GetComponent(typeof(vmControl));

                if (buttonGOScript != null)
                {
                    buttonGOScript.spawnSoda();
                }

            }
            else if ((Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, InteractRange) == true) && hit.collider.tag == "brick")
            {
                Debug.Log("Toqu� ladrillo");

                GameObject brick = GameObject.Find(hit.transform.name);

                Destroy(brick.gameObject);

            }
        }
    }

    public void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("Intermission_1"))
        {
            IM1_On = true;

            IM2_On = false;

            if (checkpointcoords == Vector3.zero)
            {
                checkpointcoords = transform.position;
            }

        }
        else if (collision.gameObject.CompareTag("Intermission_2"))
        {
            IM1_On = false;

            IM2_On = true;

            if (checkpoint2coords == Vector3.zero)
            {
                checkpoint2coords = transform.position;
            }

        }
        if (collision.gameObject.CompareTag("laser"))
        {
            HP -= 10;
            setHP();
        }
        if (collision.gameObject.CompareTag("missile"))
        {
            HP -= 15;
            setHP();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("moretime"))
        {
            gameM = GameObject.Find("GameManager");
            gameManager GMScript = (gameManager)gameM.GetComponent(typeof(gameManager));
            Debug.Log("Estoy tocando el reloj");
            GMScript.remainingTime += 15;

            Destroy(other.gameObject);
        }
    }
}

