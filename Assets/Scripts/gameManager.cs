using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class gameManager : MonoBehaviour
{
    public Text timeUI;
    public int remainingTime = 10;
    private GameObject endTrigger;
    private bool gameEnded;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(setTime());

    }

    // Update is called once per frame
    void Update()
    {
        if (remainingTime == 0)
        {
            Time.timeScale = 0f;
        }

        endTrigger = GameObject.Find("Trigger");
        triggerEndWall endScript = (triggerEndWall)endTrigger.GetComponent(typeof(triggerEndWall));

        gameEnded = endScript.gameEnd;

    }

    IEnumerator setTime()
    {

        while (remainingTime >= 0)
        {
            timeUI.text = "Time remaining: " + remainingTime;

            if (gameEnded == true)
            {
                yield return null;
            }
            else
            {
                yield return new WaitForSeconds(1.0f);
                remainingTime--;
            }

        }
    }

}
