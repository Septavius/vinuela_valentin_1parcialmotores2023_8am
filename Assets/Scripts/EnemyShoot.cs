using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShoot : MonoBehaviour
{
    private GameObject player;
    public bool reachIM1;
    private List<GameObject> enemyList;
    private Vector3 missilespawn;
    private GameObject missile;
    public GameObject missileModel;
    private Transform firepoint;

    private int missileSpeed = 100;

    public bool missileFired = false;

    // Start is called before the first frame update
    void Start()
    {
        enemyList = new List<GameObject>();

        enemyList.Add(this.gameObject);

        foreach (GameObject item in enemyList)
        {
            Transform enemy = item.transform;
            firepoint = enemy.Find("Firepoint").transform;
        }
    }

    // Update is called once per frame
    void Update()
    {
        player = GameObject.Find("Player");
        PlayerMov playerscript = (PlayerMov)player.GetComponent(typeof(PlayerMov)); //Agarro el script del jugador.
        reachIM1 = playerscript.IM1_On;

        if(reachIM1 == true)
        {
            transform.LookAt(player.transform);

            if (missileFired == false)
            {
                missilespawn = firepoint.transform.position;

                missile = Instantiate(missileModel, missilespawn, Quaternion.identity);

                missile.transform.LookAt(player.transform);

                missileFired = true;
            }

            if (missile == null)
            {
                missileFired = false;
            }

            missile.transform.Translate(Vector3.forward * missileSpeed * Time.deltaTime);

            Destroy(missile, 3);
        }



    }
    

}
