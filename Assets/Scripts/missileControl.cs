using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class missileControl : MonoBehaviour
{
    private GameObject enemy;
    public EnemyShoot fireScript;
    // Start is called before the first frame update
    void Start()
    {
        enemy = GameObject.Find("Enemy");
        fireScript = (EnemyShoot)enemy.GetComponent(typeof(EnemyShoot));
    }

    // Update is called once per frame
    void Update()
    {
        if (this.gameObject == null)
        {
            fireScript.missileFired = false;
        }
    }
}
