using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class vmControl : MonoBehaviour
{
    private GameObject soda;
    // Start is called before the first frame update
    void Start()
    {
        soda = GameObject.Find("Soda");
        soda.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void spawnSoda()
    {
        soda.gameObject.SetActive(true);
    }
}
