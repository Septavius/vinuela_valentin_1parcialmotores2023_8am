using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class triggerEndWall : MonoBehaviour
{
    public AudioSource music;
    public bool gameEnd;
    // Start is called before the first frame update
    void Start()
    {
        gameEnd = false;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if(music.isPlaying == false)
        {
            if (other.CompareTag("Player"))
            {
                music.Play();
            }
        }

        gameEnd = true;
    }
}
