using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorControl : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void doorOpen()
    {
        GameObject door = GameObject.Find("Door"); //Cuando se aprete el boton, se busca el gameobject de la puerta

        door.SetActive(false); //Se destruye la puerta

    }

}
