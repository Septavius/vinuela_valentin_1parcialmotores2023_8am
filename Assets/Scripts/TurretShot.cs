using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretShot : MonoBehaviour
{
    public GameObject lasershot;
    private List<GameObject> turretList;
    private Transform Firepoint;

    private int travelSpeed = 50;
    private bool laserGenerated;
    private Vector3 laserSpawn;
    private GameObject laser;
    public LayerMask Environment;

    // Start is called before the first frame update
    void Start()
    {
        laserGenerated = false; //Compruebo cuando se gener� un laser
        turretList = new List<GameObject>(); //Lista que va guardando cada una de las torretas

        turretList.Add(this.gameObject);

        foreach (GameObject item in turretList)
        {
            Transform TurretGO = item.transform;
            Firepoint = TurretGO.Find("Firepoint").transform; //Tomo la posici�n del ca��n donde se genera el laser
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (laserGenerated == false)
        {
            laserSpawn = Firepoint.transform.position; //Coordenadas del laser en la posici�n del ca�on

            laser = Instantiate(lasershot, laserSpawn, Quaternion.identity); //Spawneo el laser en el ca�on

            laserGenerated = true; //Se spawneo un laser
        }

        if (laserGenerated == true)
        {
            StartCoroutine(laserTravel()); //Corutina para el desplazamiento de los laseres
        }

    }

    IEnumerator laserTravel()
    {
        if (laserSpawn.x < 8)
        {
            laser.transform.position += Vector3.right * Time.deltaTime * travelSpeed; //MOV +X

            if(laser.transform.position.x > 21) //Si llega a la pared
            {
                Destroy(laser);
                laserGenerated = false;
            }
        }
        if (laserSpawn.x > 8)
        {
            laser.transform.position -= Vector3.right * Time.deltaTime * travelSpeed; // MOV -X

            if (laser.transform.position.x < -4) //Si llega a la pared
            {
                Destroy(laser);
                laserGenerated = false;
            }
        }

        yield return new WaitForSeconds(1.0f);
    }

}
